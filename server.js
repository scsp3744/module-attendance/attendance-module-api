const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();

// database name UTMSmart
mongoose.connect('mongodb+srv://root:1234@cluster0-zwd8v.azure.mongodb.net/UTMSmart?retryWrites=true&w=majority', {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
}, (err) => {
  if (err) {
    console.log('Error connecting to database.');
  }
});

app.use(morgan('dev')); // log requests in terminal
app.use(bodyParser.urlencoded({ extended: true })); // parse form
app.use(bodyParser.json()); // parse json
app.set('view engine', 'ejs'); // for serving html directly (for website part)

// allow ionic to GET/POST this server
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  next();
});
app.use(express.static('public'));

// routes for website
app.get('/lecturer', (req, res) => {
  res.render('lecturer');
});

app.get('/lecturer/attendance', (req, res) => {
  res.render('attendance');
});

app.get('/attendanceScan/:key', (req, res) => {
  Attendance.findOne({ key: req.params.key })
    .populate('Lecturer')
    .populate('CurrentCourse')
    .exec()
    .then(doc => {
      res.render('attendanceScan', {
        key: req.params.key,
        title: doc.CurrentCourse.Name,
        code: doc.CurrentCourse.Code,
        lect: doc.Lecturer.Name,
        timeStart: doc.TimeStart,
        timeStop: doc.TimeStop
      });

    });
});

app.get('/register/attendance', (req, res) => {  // localhost:8000/register/attendancje
  res.render('registerAttendance');
});

app.post('/api/register/attendance', (req, res) => {
  // POST here with {matric, key}
  console.log(req.body);
  Student.findOne({ MatricNo: req.body.matric }).exec()
    .then(std => {
      if (std) {
        Attendance.findOne({ key: req.body.key })
          .populate('Lecturer')
          .populate('CurrentCourse')
          .exec()
          .then(doc => {
            if (doc) {
              attData = {
                key: req.params.key,
                title: doc.CurrentCourse.Name,
                code: doc.CurrentCourse.Code,
                lect: doc.Lecturer.Name,
                timeStart: doc.TimeStart,
                timeStop: doc.TimeStop
              };
              var registerTime = Date.now();
              var attended;
              if (registerTime > doc.TimeStop) {
                attended = 'Late';
              }
              else {
                attended = 'Punctual';
              }
              StudentAttendance.findOne({ Attendance: doc._id, Student: std._id }).exec()
                .then(stdAtt => {
                  if (stdAtt) {
                    res.status(200).json({ msg: "Already registered" });
                  }
                  else {
                    StudentAttendance.create({
                      Attendance: doc._id,
                      Student: std._id,
                      TimeRegister: registerTime,
                      Attended: attended
                    }, (err, result) => {
                      res.status(200).json({ attData, result, std: std.Name });
                    });
                  }
                });
            }
            else {
              res.status(200).json({ msg: "Invalid Attendance Key" });

            }
          });
      }
      else {
        res.status(200).json({ msg: "Student not found" });
      }
    });
});


// routes to serve json for both ionic and website
app.get('/api/msg', (req, res) => {
  res.status(200).json({ msg: 'hello' });
});

app.post('/api/form', (req, res) => {
  console.log(req.body.testing);
  res.status(200).json({ msg: 'message received: ' + req.body.testing });
});

app.get('/api/lecturer/getCurrentCourse', (req, res) => {
  Lecturer.findOne({ Name: 'Alex' }).exec()
    .then(doc => {
      CurrentCourse.find({ Lecturer: doc._id }).exec()
        .then(docs => {
          res.status(200).json(docs);
        });
    });
});

app.post('/api/lecturer/deleteAttendance', (req, res) => {
  Attendance.deleteOne({ _id: req.body.courseID }).exec().then(doc => {
    res.json({ msg: "ok" });
  });
});

app.post('/api/lecturer/startAttendance', (req, res) => {
  console.log(req.body.courseID);

  Lecturer.findOne({ Name: 'Alex' }).exec()
    .then(doc => {
      Attendance.create({
        Lecturer: doc._id,
        CurrentCourse: req.body.courseID,
        TimeStart: Date.now(),
        TimeStop: Date.now() + 1000 * 60 * 15,
        key: makeid(6)
      }, (err, result) => {
        if (result)
          Attendance.findById(result._id)
            .populate('Lecturer')
            .populate('CurrentCourse')
            .exec()
            .then(att => {
              res.status(200).json(att);
            });
      });
    });
});

app.get('/api/lecturer/attendance', (req, res) => {
  Lecturer.findOne({ Name: 'Alex' }).exec()
    .then(doc => {
      Attendance.find({ Lecturer: doc._id })
        .populate('Lecturer')
        .populate('CurrentCourse')
        .exec()
        .then(docs => {
          res.status(200).json(docs);
        });
    });
});
app.get('/attendancelist/:key', (req, res) => {
  StudentAttendance.find({ Attendance: req.params.key })
    .populate('Student')
    .exec()
    .then(docs => {
      if (docs.length == 0) {
        return res.status(404);
      }
      res.render('attendancelist', { docs });
      console.log(docs);
    });
});
app.get('/api/lecturer/attendance/students', (req, res) => {
  StudentAttendance.find({ Attendance: req.body.att })
    .populate('Student')
    .populate('Attendance')
    .exec()
    .then(docs => {
      if (docs.length == 0) {
        return res.status(404);
      }
      return docs;
    });
});

/////////////////// req leave
app.get('/api/info/:sid', (req, res) => {  // eg: localhost:8080/api/info/5eb0431e0d1bdb11f0b7d1bb
  var sid = req.params.sid;
  console.log(sid);

  // populate will auto join other collections for data
  StudentCourse.find({ Student: sid })
    .populate('Student')
    .populate({ path: 'CurrentCourse', populate: { path: 'Lecturer' } })
    .exec()
    .then(docs => {
      if (docs.length == 0) {
        res.status(200).json({ message: 'Student ID not found' });
      }
      else {
        res.status(200).json(docs);
      }
    });
});

app.get('/api/requests/:sid', (req, res) => {
  var sid = req.params.sid;
  console.log(sid);
  RequestAbsent.find({ Student: sid }).sort({ _id: -1 })
    .populate('CurrentCourse')
    .exec()
    .then(docs => {
      res.json(docs);
    });
});

app.post('/api/requestLeave', (req, res) => {
  var sid = req.body.sid;
  var cid = req.body.cid;
  var reason = req.body.reason;
  var createdOn = Date.now();
  var requestOn = req.body.requestOn;
  console.log(sid, cid);
  StudentCourse.findOne({ Student: sid, CurrentCourse: cid }).exec()
    .then(doc => {
      if (doc) {
        RequestAbsent.create({
          Student: sid,
          CurrentCourse: cid,
          Reason: reason,
          CreatedOn: createdOn,
          RequestOn: requestOn
        }, (err, result) => {
          if (err) {
            res.status(200).json({ message: 'Error' });
          }
          else {
            res.status(200).json({ message: 'Leave requested.' });
          }
        });
      }
      else {
        res.status(200).json({ message: 'Invalid Student Course' });
      }
    });
});

app.post('/api/deleteRequest', (req, res) => {
  var sid = req.body.sid;
  var requestId = req.body.requestID;
  console.log(sid, requestId);
  RequestAbsent.deleteOne({ Student: sid, _id: requestId }).exec()
    .then((err, result) => {
      console.log(err, result);
      if (err['n'] == 0) {
        res.json({ message: "Error" });
      }
      else {
        res.json({ message: "Ok" });
      }
    });
});
///////////////////

// server start
app.listen(8080, () => console.log('app listening on port 8080'));


// mongoose models
const Attendance = mongoose.model('Attendance', {
  Lecturer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Lecturer',
    required: true
  },
  CurrentCourse: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'CurrentCourse',
    required: true
  },
  TimeStart: { type: Number, required: true },
  TimeStop: { type: Number, required: true },
  key: { type: String, required: true }
});

const StudentAttendance = mongoose.model('StudentAttendance', {
  Attendance: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Attendance',
    required: true
  },
  Student: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Student',
    required: true
  },
  TimeRegister: { type: Number, required: true },
  Attended: { type: String, required: true }
});

// demo student model from other group
const Student = mongoose.model('Student', {
  Name: { type: String, required: true },
  MatricNo: { type: String, required: true }
});

// demo course model from other group
const CurrentCourse = mongoose.model('CurrentCourse', {
  Name: { type: String, required: true },
  Code: { type: String, required: true },
  Credit: { type: Number, required: true },
  NumberOfStudent: { type: Number, required: true },
  Section: { type: Number, required: true },
  Lecturer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Lecturer',
    required: true
  }
});

// demo lecturer model from other group
const Lecturer = mongoose.model('Lecturer', {
  Name: { type: String, required: true },
});
const StudentCourse = mongoose.model('StudentCourse', {
  CurrentCourse: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'CurrentCourse',
    required: true
  },
  Student: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Student',
    required: true
  }
});

const RequestAbsent = mongoose.model('RequestAbsent', {
  Student: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Student',
    required: true
  },
  CurrentCourse: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'CurrentCourse',
    required: true
  },
  Reason: { type: String, required: true },
  CreatedOn: { type: Number, required: true },
  RequestOn: { type: Number, required: true }
});
function makeid(length) {
  var result = '';
  var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for (var i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}
