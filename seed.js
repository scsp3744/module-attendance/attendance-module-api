// const mongoose = require('mongoose');
// // mongodb+srv://root:1234@cluster0-zwd8v.azure.mongodb.net/UTMSmart?retryWrites=true&w=majority
// // mongoose.connect('mongodb://localhost/UTMSmart', {
// mongoose.connect('mongodb+srv://root:1234@cluster0-zwd8v.azure.mongodb.net/UTMSmart?retryWrites=true&w=majority', {
//   useUnifiedTopology: true,
//   useNewUrlParser: true,
//   useCreateIndex: true,
// }, (err) => {
//   if (err) {
//     console.log('Error connecting to database.');
//   }
// });

// // mongoose models
// const Attendance = mongoose.model('Attendance', {
//   Lecturer: {
//     type: mongoose.Schema.Types.ObjectId,
//     ref: 'Lecturer',
//     required: true
//   },
//   CurrentCourse: {
//     type: mongoose.Schema.Types.ObjectId,
//     ref: 'CurrentCourse',
//     required: true
//   },
//   TimeStart: { type: Number, required: true },
//   TimeStop: { type: Number, required: true },
// });

// const StudentAttendance = mongoose.model('StudentAttendance', {
//   Attendance: {
//     type: mongoose.Schema.Types.ObjectId,
//     ref: 'Attendance',
//     required: true
//   },
//   Student: {
//     type: mongoose.Schema.Types.ObjectId,
//     ref: 'Student',
//     required: true
//   },
//   Attended: { type: String, required: true }
// });

// // demo student model from other group
// const Student = mongoose.model('Student', {
//   Name: { type: String, required: true },
//   MatricNo: { type: String, required: true }
// });

// // demo course model from other group
// const CurrentCourse = mongoose.model('CurrentCourse', {
//   Name: { type: String, required: true },
//   Code: { type: String, required: true },
//   Credit: { type: Number, required: true },
//   NumberOfStudent: { type: Number, required: true },
//   Section: { type: Number, required: true },
//   Lecturer: {
//     type: mongoose.Schema.Types.ObjectId,
//     ref: 'Lecturer',
//     required: true
//   }
// });

// // demo lecturer model from other group
// const Lecturer = mongoose.model('Lecturer', {
//   Name: { type: String, required: true },
// });

// const LecturerAttendance = mongoose.model('LecturerAttendance', {
//   Lecturer: {
//     type: mongoose.Schema.Types.ObjectId,
//     ref: 'Lecturer',
//     required: true
//   },
//   PunchTime: { type: Number, required: true },
//   IsPunchIn: { type: Boolean, required: true }
// });


// // // seed dummy data

// Student.countDocuments({}, (err, count) => {
//   if (count == 0) {
//     Student.insertMany([{
//       Name: 'Abu',
//       MatricNo: 'A172'
//     }, {
//       Name: 'Bob',
//       MatricNo: 'A171'
//     }]);

//     const lecturer = new Lecturer({
//       _id: mongoose.Types.ObjectId(),
//       Name: 'Alex'
//     });

//     const currentCourse = new CurrentCourse({
//       _id: mongoose.Types.ObjectId(),
//       Name: 'Data Mining',
//       Code: 'SCSP 2123',
//       Credit: 3,
//       NumberOfStudent: 36,
//       Section: 1,
//       Lecturer: lecturer._id
//     });

//     const currentCourse2 = new CurrentCourse({
//       _id: mongoose.Types.ObjectId(),
//       Name: 'Business Intelligence',
//       Code: 'SCSP 2023',
//       Credit: 3,
//       NumberOfStudent: 36,
//       Section: 1,
//       Lecturer: lecturer._id
//     });
//     lecturer.save();
//     currentCourse.save();
//     currentCourse2.save();

//   }
// });

const mongoose = require("mongoose");

mongoose.connect('mongodb+srv://root:1234@cluster0-zwd8v.azure.mongodb.net/UTMSmart?retryWrites=true&w=majority', {
  useUnifiedTopology: true,
  useNewUrlParser: true,
  useCreateIndex: true,
}, (err) => {
  if (err) {
    console.log('Error connecting to database.');
  }
});

function mongoId() {
  var timestamp = (new Date().getTime() / 1000 | 0).toString(16);
  return timestamp + 'xxxxxxxxxxxxxxxx'.replace(/[x]/g, function () {
    return (Math.random() * 16 | 0).toString(16);
  }).toLowerCase();
};

// demo student model from other group
const Student = mongoose.model('Student', {
  Name: { type: String, required: true },
  MatricNo: { type: String, required: true }
});

const StudentCourse = mongoose.model('StudentCourse', {
  CurrentCourse: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'CurrentCourse',
    required: true
  },
  Student: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Student',
    required: true
  }
});

// demo course model from other group
const CurrentCourse = mongoose.model('CurrentCourse', {
  Name: { type: String, required: true },
  Code: { type: String, required: true },
  Credit: { type: Number, required: true },
  NumberOfStudent: { type: Number, required: true },
  Section: { type: Number, required: true },
  Lecturer: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Lecturer',
    required: true
  }
});

// demo lecturer model from other group
const Lecturer = mongoose.model('Lecturer', {
  Name: { type: String, required: true },
});

var sid1 = '5eb1d3fb4b8ffee90e1c6983';
var sid2 = '5eb1d3fbb68aa629ca8f1aab';

var lid1 = mongoId();
var lid2 = mongoId();

var cid1 = mongoId();
var cid2 = mongoId();

Student.insertMany([
  { _id: sid1, Name: 'Johnathan', MatricNo: 'A17CS0002' },
  { _id: sid2, Name: 'April', MatricNo: 'A17CS0001' }
], (err, docs) => {
  console.log('Insert Student');
});

Lecturer.insertMany([
  { _id: lid1, Name: 'Alex' },
  // { _id: lid2, Name: 'Alex' }
], (err, docs) => {
  console.log('Insert Lecturer');
});

CurrentCourse.insertMany([
  {
    _id: cid1,
    Name: 'Business Intelligence',
    Code: 'SCSP 3123',
    Credit: 3,
    NumberOfStudent: 36,
    Section: 1,
    Lecturer: lid1
  },
  {
    _id: cid2,
    Name: 'Application Development',
    Code: 'SCSP 3323',
    Credit: 3,
    NumberOfStudent: 36,
    Section: 1,
    Lecturer: lid1
  }
], (err, docs) => {
  console.log('Insert Course');
});

StudentCourse.insertMany([
  { Student: sid1, CurrentCourse: cid1 },
  { Student: sid1, CurrentCourse: cid2 },
  { Student: sid2, CurrentCourse: cid1 },
], (err, docs) => {
  console.log('Insert Student-course');
});
